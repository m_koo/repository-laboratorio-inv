#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <motion_control_msgs/JointPositions.h>
#include <geometry_msgs/Pose.h>
#include <tf/transform_datatypes.h>
#include <string>

#include <iostream>
#include <cmath>
#include <assert.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <boost/units/io.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/conversion.hpp>
#include <boost/units/systems/si/length.hpp>
#include <boost/units/systems/si/plane_angle.hpp>
#include <boost/units/io.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/conversion.hpp>

#include <Eigen/Geometry>
#include <Eigen/Core>

#include <inverseKin_youbot/computeIK.h>

using namespace std;
using namespace Eigen;

int main(int argc, char **argv)
{
  // Input required for Euler ZYZ
  double pose_x,pose_y,pose_z;
  double rz_1, ry, rz_2;
  
  // Joint commands array
  motion_control_msgs::JointPositions arm_command;
  arm_command.names.resize(5);
  arm_command.names[0].assign( "arm_link_1" );
  arm_command.names[1].assign( "arm_link_2" );
  arm_command.names[2].assign( "arm_link_3" );
  arm_command.names[3].assign( "arm_link_4" );
  arm_command.names[4].assign( "arm_link_5" );

  arm_command.positions.resize(5);
  
  // Gripper commands array
  motion_control_msgs::JointPositions gripper_command;
  gripper_command.names.resize(2);
  gripper_command.names[0].assign("gripper_finger_joint_l");
  gripper_command.names[0].assign("gripper_finger_joint_r");

  gripper_command.positions.resize(2);
  
  // Pose corresponding to Euler ZYZ
  geometry_msgs::Pose target_pose;
  
  // Init
  ros::init(argc, argv, "test_client");
  ros::NodeHandle nh_;
  
	// Service
  ros::ServiceClient client = nh_.serviceClient<inverseKin_youbot::computeIK>("inverseKin_youbot/computeIK");
  inverseKin_youbot::computeIK srv;
  
  // Message publishers
  ros::Publisher armPositionsPublisher;
  armPositionsPublisher = nh_.advertise< motion_control_msgs::JointPositions > ("arm_1/arm_controller/position_command", 10);
  
  ros::Publisher gripperPositionsPublisher;
  gripperPositionsPublisher = nh_.advertise< motion_control_msgs::JointPositions > ("arm_1/gripper_controller/position_command", 10);
  
  ros::Publisher targetPosePublisher;
  targetPosePublisher = nh_.advertise<geometry_msgs::Pose>("/target_pose",10);
  
  // Miscellaneuous variables
  int nvals, nsols, k, input_row, count;
  int line = -1;
  float homing[5] = {0.0101,  0.0101, -0.0158,  0.0222, 0.111};
  float up[5] = {2.94961, 1.1345, -2.5842,  1.7802, 2.9417};
  float joint_pos[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
  
  if (argc == 2)
  {
		std::string args(argv[1]);
		if (args == "home")
		{
      count = 0;
      while (armPositionsPublisher.getNumSubscribers() == 0 && count < 100)
      {
				printf("Waiting for subscribers...\n");
				ros::Duration(0.2).sleep();
      }
      if (armPositionsPublisher.getNumSubscribers() == 0)
      {
        ROS_ERROR("No subscribed nodes detected. Exiting\n");
        return -1;
      }
      
      arm_command.positions[0] = homing[0];
      arm_command.positions[1] = homing[1];
      arm_command.positions[2] = homing[2];
      arm_command.positions[3] = homing[3];
      arm_command.positions[4] = homing[4];
      printf( "Publishing command [%f %f %f %f %f]\n", 
              arm_command.positions[0], 
              arm_command.positions[1],
              arm_command.positions[2], 
              arm_command.positions[3],
              arm_command.positions[4]);
      
      for(int i=0; i<10; ++i)
      {
        armPositionsPublisher.publish(arm_command);
        ros::Duration(0.005).sleep();
      }
      return 0;
		}
    
    else if (args == "up")
    {
      count = 0;
      while (armPositionsPublisher.getNumSubscribers() == 0 && count < 100)
      {
				printf("Waiting for subscribers...\n");
				ros::Duration(0.2).sleep();
      }
      if (armPositionsPublisher.getNumSubscribers() == 0)
      {
        ROS_ERROR("No subscribed nodes detected. Exiting\n");
        return -1;
      }
      
      arm_command.positions[0] = up[0];
      arm_command.positions[1] = up[1];
      arm_command.positions[2] = up[2];
      arm_command.positions[3] = up[3];
      arm_command.positions[4] = up[4];
      
      printf( "Publishing command [%f %f %f %f %f]\n", 
              arm_command.positions[0], 
              arm_command.positions[1],
              arm_command.positions[2], 
              arm_command.positions[3],
              arm_command.positions[4]); 
      for(int i=0; i<10; ++i)
      {
        armPositionsPublisher.publish(arm_command);
        ros::Duration(0.005).sleep();
      }
      return 0;
    }
    else if( args == "open")
    {
      gripper_command.positions[0] = 0.01;
      gripper_command.positions[1] = 0.01;
      printf("Opening gripper\n");
      for(int i=0; i<20; ++i)
      {	
        gripperPositionsPublisher.publish(gripper_command);
        ros::Duration(0.001).sleep();
      }
    }
    else if( args == "close")
    {
      gripper_command.positions[0] = 0.0;
      gripper_command.positions[1] = 0.0;
      for(int i=0; i<10; ++i)
      {	
        gripperPositionsPublisher.publish(gripper_command);
        ros::Duration(0.005).sleep();
      }
    }
    else
    {
      ROS_ERROR("Argument Error");
      return -1;
    }
	}
	else
	{
    if (argc != 6)
    {
      ros::Duration(1).sleep();
      printf("INFO: Launch the executable with \"home\" as following argument argument to move the arm into init position.\n");
      printf("Launch the executable with \"up\" as following argument to move the arm straight up.\n");
      printf("NOTE: The arm will move without further confirmations.\n");
      printf("------------------------------------------------------\n");

      printf("Input tool goal position as a vector [PX PY PZ], then press Enter.\n");
      printf("NOTE: PX and PY are horizontal distances from joint 1 axis of rotation.\n");
      printf("PZ is tool height from GROUND level.\n");
      cin >> pose_x >> pose_y >> pose_z;
      pose_z = pose_z -0.226; 
      
      printf("Input tool orientation by describing angle of pitch of the joint 4 and roll of the joint 5 as a vector [RY RZ], then press Enter.\n");
      printf("NOTE: Since the arm lacks the sixth DOF, the first parameter of Euler ZYZ is constrained by the previously given PX PY.\n");
      cin >> ry >> rz_2;
      ry = ry;
      rz_2 = rz_2;
      rz_1 = atan2(pose_y, pose_x);
    }
    else
    {
      pose_x = strtod(argv[1], NULL);
      pose_y = strtod(argv[2], NULL);
      pose_z = ( strtod(argv[3], NULL) -0.226 ) ;
      ry = strtod(argv[4], NULL);
      rz_2 = strtod(argv[5], NULL);
      rz_1 = atan2(pose_y, pose_x);
      printf("Sending solution request Pxyz: [ %f %f %f ] Rzyz: [%f %f %f]\n", pose_x, pose_y, pose_z, rz_1, ry, rz_2);
    }
    
    Quaternion<double> target_quat =  AngleAxisd(rz_1, Vector3d::UnitZ()) * 
                                      AngleAxisd(ry, Vector3d::UnitY()) * 
                                      AngleAxisd(rz_2, Vector3d::UnitZ());
    
   //Send a POSE message to the node which will publish on RVIZ the desired target position
    target_pose.position.x = pose_x;
    target_pose.position.y = pose_y;
    target_pose.position.z = pose_z;
    target_pose.orientation.x = target_quat.x();
    target_pose.orientation.y = target_quat.y();
    target_pose.orientation.z = target_quat.z();
    target_pose.orientation.w = target_quat.w();
    for(int i=0; i<20; ++i)
    {
      targetPosePublisher.publish(target_pose);
      ros::Duration(0.001).sleep();
    }

    //Fill the SRV Request message: we will forward it as call to the service server
		srv.request.tool_pose.position.x = pose_x;
		srv.request.tool_pose.position.y = pose_y;
		srv.request.tool_pose.position.z = pose_z;
		srv.request.tool_pose.orientation.x = target_quat.x();
		srv.request.tool_pose.orientation.y = target_quat.y();
		srv.request.tool_pose.orientation.z = target_quat.z();
		srv.request.tool_pose.orientation.w = target_quat.w();
		 
		if (client.call(srv))
	  {   
			nvals = srv.response.joint_values.size();
	    nsols = nvals/6;
      Eigen::MatrixXd response(nsols,6);
	    
	    k = 0;
	    while(k<nvals)
	    {
	      for (int i=0; i<nsols; ++i)
	      {
	        for (int j=0; j<6; ++j)
	        { 
	          response(i,j) = srv.response.joint_values[k];
	          ++k;
	        }
	      } 
	    }
	    cout << "Solutions found: " << nsols << std::endl << response << std::endl;
	
			if (nsols > 0)
			{
        printf("Choose solution identifier:\n");
        while (line == -1)
        {
          printf("0: direct space, low elbow\n1: direct space, high elbow\n2: inverse space, high elbow\n3: inverse space, low elbow\n");
          cin >> input_row;
          for (k=0; k<nsols; ++k)
          {
            if (input_row == response(k,5) )
            {
              line = k;
              k = nsols+1;
            }          
          }
          if ( line == -1)
          {
            printf("Wrong input. Choose an existing solution identifier\n");
          }
        }
        for (int k=0; k<5; ++k)
        {
          joint_pos[k] = response(line,k);
        }
        
        arm_command.positions[0] = joint_pos[0];
        arm_command.positions[1] = joint_pos[1];
        arm_command.positions[2] = joint_pos[2];
        arm_command.positions[3] = joint_pos[3];
        arm_command.positions[4] = joint_pos[4];
        
        printf("Currently subscribed nodes: %d\n", armPositionsPublisher.getNumSubscribers());
        count = 0;
				while (armPositionsPublisher.getNumSubscribers() < 1 && count < 100)
				{
					printf("Waiting for subscribers...\n");
					ros::Duration(0.1).sleep();
				}
        if (armPositionsPublisher.getNumSubscribers() == 0)
        {
          ROS_ERROR("No subscribed nodes detected. Exiting\n");
          return -1;
        }
				printf( "Publishing command [%f %f %f %f %f]\n", 
              arm_command.positions[0], 
              arm_command.positions[1],
              arm_command.positions[2], 
              arm_command.positions[3],
              arm_command.positions[4]); 
              
				for(int i=0; i<5; ++i)
				{
					armPositionsPublisher.publish(arm_command);
          ros::Duration(0.001).sleep();
				}
        
			}
			
	  }
	  else
	  {
	    ROS_ERROR("Failed to call service inverseKin_youbot/computeIK");
	    return -1;
	  }
	
	  return 0;
	}
}

