#include <ros/ros.h>
#include <geometry_msgs/Pose.h>

#include <iostream>
#include <assert.h>
#include <cmath>

#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

#include <boost/shared_ptr.hpp>

boost::shared_ptr<unsigned int> publish_tf;
boost::shared_ptr<tf::TransformBroadcaster> br;
boost::shared_ptr<tf::Transform> transform;

void targetPoseCallback(const geometry_msgs::Pose::ConstPtr& msg)
{
  *publish_tf=1;
  transform->setOrigin( tf::Vector3(  msg->position.x,
                                      msg->position.y, 
                                      msg->position.z ) );
  transform->setRotation( tf::Quaternion( msg->orientation.x, 
                                          msg->orientation.y,
                                          msg->orientation.z,
                                          msg->orientation.w) );
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "target_pose_broadcaster");

  publish_tf.reset( new unsigned int );
  br.reset(new tf::TransformBroadcaster );
  transform.reset( new tf::Transform );

  ros::NodeHandle nh_;
  ros::Rate rate(50);
  ros::Subscriber subToPose = nh_.subscribe("/target_pose", 10, &targetPoseCallback);

  *publish_tf=0;
  
  //transform->setOrigin( tf::Vector3( 0, 0, 0 ) );
  //transform->setRotation( tf::Quaternion( 0, 0, 0 ) );

  while (ros::ok())
  {
    if (*publish_tf == 1)
    {
      br->sendTransform(tf::StampedTransform(*transform, ros::Time::now(), "/ik_origin", "/target_pose"));
    }
    ros::spinOnce();
    rate.sleep();
  }
  return 0;
};
