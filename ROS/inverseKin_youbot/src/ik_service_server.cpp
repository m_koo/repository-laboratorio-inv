#include <ros/ros.h>

#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Pose.h>

#include <iostream>
#include <cmath>

#include <Eigen/Geometry>
#include <Eigen/Core>

#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

#include <inverseKin_youbot/computeIK.h>
#include <inverseKinSolver.h>

using namespace std;
using namespace Eigen;

ros::Time call_t;
ros::Time compute_t;
ros::Duration busy_t;

bool computeIK(inverseKin_youbot::computeIK::Request &req, inverseKin_youbot::computeIK::Response &res)
{
  // create Eigen quaternion from ROS Pose quaternion message
  // Eigen allows us to easily manipulate homogeneous transforms required by the internal IK solver
  Quaternion<double> target_quat = Eigen::Quaternion<double>(req.tool_pose.orientation.w,req.tool_pose.orientation.x, req.tool_pose.orientation.y, req.tool_pose.orientation.z);

  // create Eigen Transform from Eigen quaternion and Eigen position vector
  Transform< double, 3, Affine, RowMajor> eigenPose = Transform< double, 3, Affine, RowMajor>();
  eigenPose.linear() = target_quat.toRotationMatrix();
  eigenPose.translation() = Vector3d(req.tool_pose.position.x, req.tool_pose.position.y,req.tool_pose.position.z);
  
  // create solution
  Eigen::VectorXd solution(6);
  // assignment to improve call time (-80 microseconds)
  solution.setZero();
  
  // array containing the solutions
  std::vector<double> sol_container;
    
  for (int k=0; k<4; ++k)
  {
    // compute a specific solution
    call_t = ros::Time::now();
    
    solution = solve_ik(eigenPose, k);
    
    compute_t = ros::Time::now(); 
    
    busy_t = compute_t - call_t;
    cout << "Time required for computation of solution " << k <<": " << (busy_t.toSec())*(double)1000.0 << " milliseconds.\n\n";

    // if it's OK (> 0.0) memorize it into the solutions array
    if (solution(0) > 0.0)
    {
      for (int i=0; i<6; ++i)
      {
        sol_container.push_back(solution(i));
      }
    } 
  }
  
  cout << "----------------------------------------------------------" << std::endl;
  // resize the joint_values message service to the size of the solutions container
  res.joint_values.resize(sol_container.size());
  for(unsigned int k=0; k < sol_container.size(); ++k)
  {
    res.joint_values[k] = sol_container[k];
  }
  return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "computeIK_server");
  ros::NodeHandle n;
  ros::ServiceServer service = n.advertiseService("inverseKin_youbot/computeIK", computeIK);

  ROS_INFO("Ready to compute IK solution for desired tool pose.");
  ros::spin();
  return 0;
}
