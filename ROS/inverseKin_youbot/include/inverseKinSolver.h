#include <iostream>
#include <cmath>

#include <Eigen/Geometry>
#include <Eigen/Core>

#define DEG2RAD M_PI/180

using namespace Eigen;
using namespace std;

Eigen::VectorXd solve_ik( Transform< double, 3, Affine, RowMajor> eigenPose, int sol_index );
