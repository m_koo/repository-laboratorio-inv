#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"

double current_position;
double vel_cmd;

void odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  current_position = msg->pose.pose.position.x;
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "control");

  ros::NodeHandle n;

  current_position = 0;
  geometry_msgs::Twist vel_cmd;

  ros::Subscriber sub = n.subscribe("/odom", 1000, odomCallback);
  ros::Publisher pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1000);

  ros::Rate loop_rate(10);  //10 Hz!!  

  while (ros::ok())
  {
    //Loop di controllo
    // Vel = K * (Pos_Desired - Pos_Current)   
    vel_cmd.linear.x = 0.3 * (1 - current_position);
    pub.publish(vel_cmd);

    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
