//// 1. Configurazione Ambiente ROS 

Per poter configurare correttamente il framework ROS, occorre settare alcune
variabili d'ambiente. Nel proprio bashrc (~/.bashrc) si devono aggiungere le seguenti
righe:

#Setup Variabili d'ambiente generali ROS
source /opt/ros/fuerte/setup.bash
#Setup Variabili d'ambiente OROCOS
source /opt/ros/fuerte/stacks/orocos_toolchain/env.sh
#Configurazione del proprio workspace ROS
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:/percorso/della/tua/home
#Indirizzo del Master ROS
export ROS_MASTER_URI=http://localhost:11311

# LUA-SETUP
if [ "x$LUA_PATH" == "x" ]; then LUA_PATH=";;"; fi
if [ "x$LUA_CPATH" == "x" ]; then LUA_CPATH=";;"; fi

export LUA_PATH="$LUA_PATH;`rospack find ocl`/lua/modules/?.lua"
export LUA_PATH="$LUA_PATH;`rospack find kdl`/?.lua"
export LUA_PATH="$LUA_PATH;`rospack find rFSM`/?.lua"
export LUA_PATH="$LUA_PATH;`rospack find rttlua_completion`/?.lua"
#export LUA_PATH="$LUA_PATH;`rospack find youbot_master_rtt`/lua/?.lua"
export LUA_PATH="$LUA_PATH;`rospack find kdl_lua`/lua/?.lua"
export LUA_PATH="$LUA_PATH;`rospack find uMF`/?.lua"

export LUA_CPATH="$LUA_CPATH;`rospack find rttlua_completion`/?.so"

export PATH="$PATH:`rosstack find orocos_toolchain`/install/bin"
# END LUA-SETUP

//// 2. Procedura per avviare la simulazione

1. Avviare il ros master tramite il comando
  $ roscore

2. Avviare il robot_state_publisher per lo youbot
  $ roslaunch youbot_oodl youbot_joint_state_publisher.launch

3. Avviare il simulatore V-REP
  $ cd /opt/V-REP.../
  $ ./vrep.sh

4. In V-REP, andare su File -> Open Scene -> e selezionare la scena
   'youbot_arena.ttt' che trovate nella cartella 'vrep_scenes' del 
   repository

5. Avviate la simulazione cliccando sull'icona PLAY


