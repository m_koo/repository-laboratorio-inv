// g++ -I/usr/include/eigen3 -o geometricJacobian geometricJacobian.cpp
#include <stdio.h>
#include <iostream>
#include <math.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#define _USE_MATH_DEFINES

using namespace std;
using namespace Eigen;

Matrix<double,6,5> computeJacobian( VectorXd q )
{
  Matrix<double,6,5> JacG;
  Matrix<double,5,4> DH;
  Transform< double, 3, Affine, RowMajor> A, A01, A12, A23, A34, A45, A02, A03, A04, A05;
  double theta, d, a, alpha;
  Vector3d JP, JO, z, p;
  
  // init data
  // ---------------  
  //           theta  d         a         alpha
  DH.row(0) << 0.0,   0.0147,   0.0330,   M_PI/2;
  DH.row(1) << 0.0,   0.0,      0.1550,   0.0;
  DH.row(2) << 0.0,   0.0,      0.1350,   0.0;
  DH.row(3) << 0.0,   0.0,      0.0,      M_PI/2;
  DH.row(4) << 0.0,   0.2175,   0.0,      0.0;
  
  // build component transform matrices
  // ---------------
  theta = q(0);
  d     = DH(0,1);
  a     = DH(0,2);
  alpha = DH(0,3);

  A.linear().row(0) << cos(theta), -sin(theta)*cos(alpha),  sin(theta)*sin(alpha);
  A.linear().row(1) << sin(theta), cos(theta)*cos(alpha),   -cos(theta)*sin(alpha);
  A.linear().row(2) << 0,          sin(alpha),              cos(alpha);
  A.translation() << a*cos(theta), a*sin(theta),            d;

  A01 = A;
  
  // 1-2 (2)
  theta = q(1);
  d     = DH(1,1);
  a     = DH(1,2);
  alpha = DH(1,3);

  A.linear().row(0) << cos(theta), -sin(theta)*cos(alpha),  sin(theta)*sin(alpha);
  A.linear().row(1) << sin(theta), cos(theta)*cos(alpha),   -cos(theta)*sin(alpha);
  A.linear().row(2) << 0,          sin(alpha),              cos(alpha);
  A.translation() << a*cos(theta), a*sin(theta),            d;

  A12 = A;
  
  // 2-3 (3)
  theta = q(2);
  d     = DH(2,1);
  a     = DH(2,2);
  alpha = DH(2,3);

  A.linear().row(0) << cos(theta), -sin(theta)*cos(alpha),  sin(theta)*sin(alpha);
  A.linear().row(1) << sin(theta), cos(theta)*cos(alpha),   -cos(theta)*sin(alpha);
  A.linear().row(2) << 0,          sin(alpha),              cos(alpha);
  A.translation() << a*cos(theta), a*sin(theta),            d;

  A23 = A;
  
  // 3-4 (4)
  theta = q(3);
  d     = DH(3,1);
  a     = DH(3,2);
  alpha = DH(3,3);

  A.linear().row(0) << cos(theta), -sin(theta)*cos(alpha),  sin(theta)*sin(alpha);
  A.linear().row(1) << sin(theta), cos(theta)*cos(alpha),   -cos(theta)*sin(alpha);
  A.linear().row(2) << 0,          sin(alpha),              cos(alpha);
  A.translation() << a*cos(theta), a*sin(theta),            d;

  A34 = A;
  
  // 4-5 (5)
  theta = q(4);
  d     = DH(4,1);
  a     = DH(4,2);
  alpha = DH(4,3);

  A.linear().row(0) << cos(theta), -sin(theta)*cos(alpha),  sin(theta)*sin(alpha);
  A.linear().row(1) << sin(theta), cos(theta)*cos(alpha),   -cos(theta)*sin(alpha);
  A.linear().row(2) << 0,          sin(alpha),              cos(alpha);
  A.translation() << a*cos(theta), a*sin(theta),            d;

  A45 = A;       
  
    
  // Create partial chain transform matrices
  // ---------------
  
  A02 = A01*A12;
  A03 = A02*A23;
  A04 = A03*A34;
  A05 = A04*A45;
  
  // Create Jacobian
  // ---------------
  
  p = A05.translation();
  
  // (i=1)
  // JP1 = [z_0 x (p - p_0)]
  // JO1 = [z_0]
  // Caso particolare: z_0
  // z0 appartiene a A0 (non dichiarata), matrice di cambio di base
  // Siccome l'origine del sistema di riferimento coincide con la base
  // del robot (giunto 1), A0 è I, quindi z_0 è [0 0 1]^T
  
  z << 0, 0, 1;
  
  // p_0 è nullo
  JP = z.cross( p ) ;
  JO = z;
  
  JacG(0,0) = JP(0);
  JacG(1,0) = JP(1);
  JacG(2,0) = JP(2);
  JacG(3,0) = JO(0);
  JacG(4,0) = JO(1);
  JacG(5,0) = JO(2);

  // (i=2)
  // JP2 = [z_1 x (p - p_1)]
  // JO2 = [z_1]
  z = A01.linear().col(2);
  JP = z.cross( p - A01.translation() );
  JO = z;
  
  JacG(0,1) = JP(0);
  JacG(1,1) = JP(1);
  JacG(2,1) = JP(2);
  JacG(3,1) = JO(0);
  JacG(4,1) = JO(1);
  JacG(5,1) = JO(2);
  
  // (3)
  // JP3 = [z_2 x (p - p_2)]
  // JO3 = [z_2]
  z = A02.linear().col(2);
  JP = z.cross( p - A02.translation() );
  JO = z;
  
  JacG(0,2) = JP(0);
  JacG(1,2) = JP(1);
  JacG(2,2) = JP(2);
  JacG(3,2) = JO(0);
  JacG(4,2) = JO(1);
  JacG(5,2) = JO(2);
  
  // (4)
  // JP4 = [z_3 x (p - p_3)]
  // JO4 = [z_3]
  z = A03.linear().col(2);
  JP = z.cross( p - A03.translation() );
  JO = z;
  
  JacG(0,3) = JP(0);
  JacG(1,3) = JP(1);
  JacG(2,3) = JP(2);
  JacG(3,3) = JO(0);
  JacG(4,3) = JO(1);
  JacG(5,3) = JO(2);
  
  // (5)
  // JP5 = [z_4 x (p - p_4)]
  // JO5 = [z_4]
  z = A04.linear().col(2);
  JP = z.cross( p - A04.translation() );
  JO = z;
  
  JacG(0,4) = JP(0);
  JacG(1,4) = JP(1);
  JacG(2,4) = JP(2);
  JacG(3,4) = JO(0);
  JacG(4,4) = JO(1);
  JacG(5,4) = JO(2);
  
  return JacG;
  
}

int main(int argc, char** argv)
{
  Matrix<double,6,5> geomJac;
  
  if( argc != 6)
  {
    cout << "Error: input joint values after executable name" << endl;
    return -1;
  }
  
  
  VectorXd q(5);
  VectorXd qd(5);
  
  q << atof(argv[1]), atof(argv[2]), atof(argv[3]), atof(argv[4])+M_PI_2, atof(argv[5]);
  qd << 1, 1, 1, 1, 1;
  geomJac = computeJacobian(q);
  
  for(int m=0; m<6; ++m)
  {
    for(int n=0; n<5; ++n)
    {
      if ( abs(geomJac(m,n)) < 1e-08) 
      {
        geomJac(m,n) = 0.0;
      }
    }
  }
  
  cout << "----------------------------" << endl;
  cout << "Geometric Jacobian for joint values q:" << endl << "[" << q.transpose() << "]" << endl;
  cout << "----------------------------" << endl;
  cout << geomJac << endl;
  cout << "----------------------------" << endl;
  cout << "Applying the joint velocities (rad/s):" << endl << "[" << qd.transpose() << "]" << endl;
  cout << "----------------------------" << endl;
  cout << "We obtain an end-effector instant twist of:" << endl << "[" << (geomJac*qd).transpose() << "]" << endl;
  cout << "----------------------------" << endl;  
  cout << "Note: Twist is defined as [linear velocity relative to base x,y,z; angular velocity relative to base x,y,z]" << endl;
  
  return 0;
  
}

