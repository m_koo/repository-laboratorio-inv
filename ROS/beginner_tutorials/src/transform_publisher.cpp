#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <cmath>

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <Eigen/Geometry>
#include <Eigen/Core>

using namespace std;
using namespace Eigen;


int main(int argc, char** argv)
{
  
  // Inizializzazione di ROS
  ros::init(argc, argv, "tf_publisher");
  ros::NodeHandle nh_;
  ros::Rate rate(50);
  
  // Strutture dati ROS contenenti le trasformazioni padre-figlio
  tf::TransformBroadcaster br;
  tf::Transform tf0, tf1; 
  
  // -----------------------------------
  // 1. Eigen: Creiamo le trasformazioni 
  // ----------------------------------- 
  //Transform< double, 3, Affine, RowMajor> T0, T1, A01;
  
  // T0, frame "padre"
  // linear() seleziona la parte rotazionale della Transform
  // row(k) seleziona la riga k-esima
  T0.linear().row(0) << 1,  0,    0;
  T0.linear().row(1) << 0,  1,    0;
  T0.linear().row(2) << 0,  0,    1;
  // translation() seleziona la parte traslazionale della Transform
  T0.translation() << 0.0, 0.0, 0.0;
  
  // A01, matrice di trasformazione di coordinate da T0 a T1
  A01.linear().row(0) << -1,  0,     0;
  A01.linear().row(1) << 0,   -1,    0;
  A01.linear().row(2) << 0,   0,     1;
  A01.translation() = Vector3d(1, 0.5, 0);
  
  // T1, T0*A01
  T1 = T0*A01;
  
  // --------------------------------------------------------------------
  // 2. Conversione: convertiamo le trasformazioni Eigen nei tipi di dati
  //    richiesti dai messaggi tf ROS
  // --------------------------------------------------------------------
  
  // ROS opera su quaternioni, possiamo sempre usare Eigen per istanziarli
  Quaternion<double> T0Q( T0.linear() );
  Quaternion<double> T1Q( T1.linear() );

  // Riempimento delle strutture dati ROS con i dati Eigen
  tf0: trasformazione equivalente a T0
  tf0.setOrigin( tf::Vector3( T0.translation().x(), 
                              T0.translation().y(), 
                              T0.translation().z()) );
  tf0.setRotation( tf::Quaternion(  T0Q.x(), 
                                    T0Q.y(), 
                                    T0Q.z(), 
                                    T0Q.w()) );

  // tf1: trasformazione equivalente a T1
  tf1.setOrigin( tf::Vector3( T1.translation().x(), 
                              T1.translation().y(), 
                              T1.translation().z()) );
  tf1.setRotation( tf::Quaternion(  T1Q.x(), 
                                    T1Q.y(), 
                                    T1Q.z(), 
                                    T1Q.w()) );

  // Pubblicazione dei messaggi
  while (ros::ok())
  {
    br.sendTransform(tf::StampedTransform(tf1, ros::Time::now(), "/Transform_0", "/Transform_1"));
    ros::spinOnce();
    rate.sleep();
  }
  return 0;
  

};



