function draw_frame(T, area, multi)
%DRAW_FRAME Utility tool to draw frame representation as orientation axes
%   
%   DRAW_FRAME(T, SCALING, AREA, MULTI) plots a sequence of axes terns 
%   corresponding to every homogeneous transform contained in T, which can
%   be single (4x4) or an array of transforms (4x4xj).
%
%   AREA is the total representation area (frame origin at AREA/2)
%
%   MULTI is a flag(0 , 1). Set to True if T is a transform sequence, and
%   to False if T is just a single homogeneous transform.
%

    semiarea = area/2;
    scaling = area/5;
    if multi ~= 1
        Px = T(1,4);
        Py = T(2,4);
        Pz = T(3,4);
        T(1:3,1:3) = T(1:3,1:3) ./ (1/scaling);


        plot3(  [Px Px+T(1,1)], [Py Py+T(2,1)], [Pz Pz+T(3,1)],'r-', ...
                [Px Px+T(1,2)], [Py Py+T(2,2)], [Pz Pz+T(3,2)],'g-', ...
                [Px Px+T(1,3)], [Py Py+T(2,3)], [Pz Pz+T(3,3)],'b-', 'LineWidth', 2  );
        % view([-90 0])
        axis on
        axis equal
        axis([-semiarea semiarea -semiarea semiarea -semiarea semiarea])
        grid on
        hold on
        plot3(0,0,0,'r.')
        plot3(  [0 semiarea/5], [0 0], [0 0],'r-', ...
                [0 0], [0 semiarea/5], [0 0],'g-', ...
                [0 0], [0 0], [0 semiarea/5],'b-');
        %title('Position: %f %f %f', Px, Py, Pz);
        hold off

    else
        for k=1:size(T,3)
            Px = T(1,4,k);
            Py = T(2,4,k);
            Pz = T(3,4,k);
            T(1:3,1:3,k) = T(1:3,1:3,k) ./ (1/scaling);


            plot3(  [Px Px+T(1,1,k)], [Py Py+T(2,1,k)], [Pz Pz+T(3,1,k)],'r-', ...
                    [Px Px+T(1,2,k)], [Py Py+T(2,2,k)], [Pz Pz+T(3,2,k)],'g-', ...
                    [Px Px+T(1,3,k)], [Py Py+T(2,3,k)], [Pz Pz+T(3,3,k)],'b-');
            % view([-90 0])
            axis on
            axis equal
            axis([-semiarea semiarea -semiarea semiarea -semiarea semiarea])
            grid on
            hold on
            plot3(0,0,0,'r.')
            plot3(  [0 semiarea/10], [0 0], [0 0],'r-', ...
            [0 0], [0 semiarea/10], [0 0],'g-', ...
            [0 0], [0 0], [0 semiarea/10],'b-');
            %update = sprintf('Position of frame %d of %d: %f %f %f', k, size(T,3), Px, Py, Pz);
            %title(update);
            hold off 
            pause(0.05)
        end
    end
end