function [Q yQ] = youbotInverseKin( G )
% YOUBOTINVERSEKIN Summary of this function goes here
% Detailed explanation goes here
           


% Load DH table and get physical arm parameters
youbot_DH;
L2 = abs(youbotDH(2,3));
L3 = abs(youbotDH(3,3));
L4 = abs(youbotDH(5,2));

% Solution vector (Each row is a solution. Two implementations: High
% elbows)
Q = zeros(2,5);


% Generic parameters and considerations

% calculation of beta and theta
beta = atan2(G(3,3), sqrt( G(1,3)^2 + G(2,3)^2 ));

% INFO 
% theta angles = "internal" angles used by trigonometry calculations; 
% q angles = rads from "straight" configuration

% INFO
% if abs(cos_theta3) gets > 1, the solution is not valid

%               .
%              /
%             /
%   theta(i) /   q(i)
% __________o_  _  _  _

% ================



% ================
% direct arm space
% ================

q1 = atan2(G(2,4), G(1,4));
Z1 = G(3,4) - youbotDH(1,2);
Z2 = Z1 - L4*sin(beta);
X1 = pdist([0 0; G(1,4) G(2,4)]) - youbotDH(1,3);
X2 = X1 - L4*cos(beta);

cos_theta3 = ( -Z2^2 -X2^2 + L2^2 + L3^2)/(2*L2*L3);

if cos_theta3 > -1 && cos_theta3 < 1

    % negative sin (high elbow)
    sin_theta3 = -sqrt(1-cos_theta3^2);
    theta3 = atan2(sin_theta3, cos_theta3);
    q3 = - pi - theta3;
    k1 = L2 + L3*cos(q3);
    k2 = L3 * sin(q3);
    theta2 = atan2(Z2,X2) - atan2(k2,k1);
    q2 = theta2;
    q4 = - q2 - q3 + beta;
    
    % joint 5: calculate rotation difference around versor Z of guess and
    % versor Z of forward kinematics till 4th joint
    E = youbot.fkine([q1 q2 q3 q4 0]);
    R = transp(E(1:3,1:3))*G(1:3,1:3);
    q5 = atan2(R(2,1),R(1,1));
    
    % Fill up
    Q(1,:) = [q1 q2 q3 q4 q5];
else
    Q(1,:) = [NaN NaN NaN NaN NaN];
end


% =======================


% =================
% inverse arm space
% =================

q1 = atan2(G(2,4), G(1,4))-pi;
Z1 = G(3,4) - youbotDH(1,2);
Z2 = Z1 - L4*sin(beta);
X1 = pdist([0 0; G(1,4) G(2,4)]) + youbotDH(1,3);
X2 = X1 - L4*cos(beta);

cos_theta3 = ( -Z2^2 -X2^2 + L2^2 + L3^2)/(2*L2*L3);

if cos_theta3 > -1 && cos_theta3 < 1

    % positive sin (high elbow inverse)
    sin_theta3 = sqrt(1-cos_theta3^2);
    theta3 = atan2(sin_theta3, cos_theta3);
    q3 = pi - theta3;
    k1 = L2 + L3*cos(q3);
    k2 = L3 * sin(q3);
    theta2 = atan2(Z2,X2) + atan2(k2,k1);
    q2 = pi - theta2;
    q4 = pi - q2 - q3 - beta;
    E = youbot.fkine([q1 q2 q3 q4 0]);
    R = transp(E(1:3,1:3))*G(1:3,1:3);
    q5 = atan2(R(2,1),R(1,1));
    Q(2,:) = [q1 q2 q3 q4 q5];
else
    Q(2,:) = [NaN NaN NaN NaN NaN];
end

end

