function varargout = DHview(varargin)
%DHview Denavit-Hartenberg plotter based on Peter Corke's Robotics Toolbox
%   
%   DHview(DH, BASE_TRANSFORM) draws the robot structure described in 
%   the Denavit-Hartenberg parameters matrix DH.
%   BASE_TRANSFORM (homogeneous transform matrix) is usually set to eye(4).
%
%   DH's columns order must be: theta, d, a, alpha.
%
%   BASE_TRANSFORM is needed only if robot origin is different from world
%   origin.
%

% Edit the above text to modify the response to help DHview

% Last Modified by GUIDE v2.5 26-Mar-2012 15:54:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DHview_OpeningFcn, ...
                   'gui_OutputFcn',  @DHview_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DHview is made visible.
function DHview_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
DHM = varargin{1};
BaseT = varargin{2};
guidata(hObject, handles);
set(handles.DHMatrix, 'Data', DHM);
set(handles.base_transform_table,'Data', BaseT);


function varargout = DHview_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in structureButton.
function structureButton_Callback(hObject, eventdata, handles)
h = figure('Name', 'Robot', 'Nextplot', 'replace');
DHM = get(handles.DHMatrix, 'Data');
BaseT = get(handles.base_transform_table,'Data');
[r c] = size(DHM);
q = DHM(1:r, 1)';
dbot = SerialLink(DHM);
dbot.base = BaseT;
plot(dbot, q, 'jaxes', 'joints');

viewButton_Callback(hObject,eventdata,handles)


% --- Executes on button press in viewButton.
function viewButton_Callback(hObject, eventdata, handles)
DHM = get(handles.DHMatrix, 'Data');
BaseT = get(handles.base_transform_table,'Data');
[r c] = size(DHM);
q = DHM(1:r, 1)';
dbot = SerialLink(DHM);
dbot.base = BaseT;
%set(gcf,'nextplot','replacechildren');
%set(gcf,'nextplot','new');
%set(gcf,'nextplot','add');
plot(dbot, q, 'jaxes', 'joints');
hold off


% --- Executes on button press in resetButton.
function resetButton_Callback(hObject, eventdata, handles)
% hObject    handle to resetButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(gcf)
