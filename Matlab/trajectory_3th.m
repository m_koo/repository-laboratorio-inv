function [xsol xfun] = trajectory_3th(time, start_c, end_c)
    %time = seconds
    %start = [q(0) qd(0)]
    %end = [q(t) qd(t)]
    
    b = [start_c end_c]';
    
    syms x;
    A = [   0       0       0       1; 
            0       0       1       0; 
            x^3     x^2     x       1;
            3*x^2   2*x     1       0];
        
    
    Aq = subs(A,x,time);
    
    xsol = linsolve(Aq, b);

    xfun = [];
    
    
    for k=1:4
        sign_coeff = num2str(xsol(k));
        if sign_coeff >=0
            sign_coeff = strcat('+',sign_coeff);
        end
        xfun = strcat(xfun,sign_coeff,'*x.^',num2str(4-k));
    end
    
    close all
    
    % function for trajectory in the considered interval
    xfun = strcat(xfun,';');
    fd = diff(sym(xfun));
    fdd = diff(sym(fd));
    
    % function for position trajectory before interval
    fb = strcat(num2str(start_c(2)),'*x+');
    fb = strcat(fb,num2str(start_c(1)));
    
    % function for position trajectory after interval
    fa = strcat(num2str(end_c(2)),'*x+');
    fa = strcat(fa,num2str(end_c(1)));
    
    % linspaces array for function (xt), before function (xb), after
    % function (xa)
    xt = linspace(0,time,(time*50)+1);
    xb = linspace(-1,0,11);
    xa = linspace(time, time+1, 11);
    
    % ========
    % POSITION
    % ========
    figure(1);
    % function with . before operators
    % we need xfun to differentiate it with syms, but syms doesn't allow "."
    qf = strrep(xfun,'*','.*');
    qf = strrep(qf,'^','.^');
    
    % function calc
    % -------------
    x = xt;
    % y array; eval
    yt = eval(qf);
    
    % out-of-interval lines
    % ---------------------
    % before
    x = linspace(-1,0,11);
    yb = eval(fb);
    % after
    x = linspace(0,1,11);
    ya = eval(fa);
    
    % plot function and hold on
    plot(xb,yb,'r-',xt,yt,'b-',xa,ya,'r-');
    hold on
    
    % window decoration
    axis([-1 time+1 min(yt)-2 max(yt)+2]);
    legend('q(t)');
    ylabel('rad');
    grid on
    
    % ========
    % VELOCITY
    % ========
    figure(2);
    
    % convert sym to string to use strrep
    fdt = char(fd);
   
    % add . operators  
    fdt = strrep(fdt,'*','.*');
    fdt = strrep(fdt,'^','.^');

    % x array
    x = xt;
    
    % y array; eval
    ydt = eval(fdt);
    
    % horizontal lines
    % before
    ydb = linspace(start_c(2),start_c(2),max(size(xb)));
    
    % after
    yda = linspace(end_c(2),end_c(2),max(size(xa)));
    
    % plot
    plot(xb,ydb,'r-', xt,ydt,'b-', xa,yda,'r-');
    hold on
    
    
    % window decoration
    axis([-1 time+1 min(ydt)-2 max(ydt)+2]);
    legend('qd(t)');
    ylabel('rad/s');
    grid on
    
    % ============
    % ACCELERATION
    % ============
    figure(3);
    
    % convert sym to string to use strrep
    fddt = char(fdd);
    
    % add . operators  
    fddt = strrep(fddt,'*','.*');
    fddt = strrep(fddt,'^','.^');

    % y array; eval
    yddt = eval(fddt);
    
    % horizontal lines
    % before
    yddb = linspace(0,0,max(size(xb)));
    % after
    ydda = linspace(0,0,max(size(xa)));
    
    % plot
    plot(xb,yddb,'r-', xt,yddt,'b-', xa,ydda,'r-');
    hold on
    
    % discontinuity lines
    line([0 0],[yddt(1) 0]);
    line([time time],[yddt(end) 0]);
    
    % window decoration
    axis([-1 time+1 min(yddt)-2 max(yddt)+2]);
    legend('qdd(t)');
    ylabel('rad/s^2');
    grid on
    
    hold off
    
end