% d1 values
% ---------
% 0.147 = physical vertical distance from arm base to joint 2
% 0.019 = vertical distance from arm_link_1 to arm_link_2
% 0.161 = vertical distance from base_link to arm_link_2

%                                     _
% 0.2450      arm_link_2               |   
%                             0.019    |
% 0.226       arm_link_1               | 
%                             0.096    |-- 0.147 arm physical height from its base to joint 2       
% 0.13        arm_link_0               |
%                             0.046   _|
% 0.084       base_link                | 
%                             0.084    |-- 0.0980
% 0           ground                  _|

youbotDH = [    0       0.147      0.0330      pi/2
                0       0          0.1550      0
                0       0          0.1350      0
                0       0          0           pi/2
                0       0.2175     0           0      ];     
     
youbot = SerialLink(youbotDH);
youbot.base = eye(4);
youbot.offset =  [0 0 0 pi/2 0];
