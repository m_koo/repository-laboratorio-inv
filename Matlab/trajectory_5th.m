function [xsol xfun] = trajectory_5th(time, start_c, end_c)
    %time = seconds
    %start = [q(0) qd(0) qd(0)]
    %end = [q(t) qd(t) qdd(t)]
    b = [start_c end_c]';
    
    syms x;
    A = [   0       0       0       0       0       1; 
            0       0       0       0       1       0; 
            0       0       0       2       0       0; 
            x^5     x^4     x^3     x^2     x       1; 
            5*x^4   4*x^3   3*x^2   2*x     1       0; 
            20*x^3  12*x^2  6*x     2       0       0];

    Aq = subs(A,x,time);
    
    xsol = linsolve(Aq, b);

    xfun = [];
    
    
    for k=1:6
        sign_coeff = num2str(xsol(k));
        if sign_coeff >=0
            sign_coeff = strcat('+',sign_coeff);
        end
        xfun = strcat(xfun,sign_coeff,'*x^',num2str(6-k));
    end
   
    % function for trajectory in the considered interval
    xfun = strcat(xfun,';');
    fd = diff(sym(xfun));
    fdd = diff(sym(fd));
    
    % function for acceleration trajectory before interval
    fddb = strcat('0*x+',num2str(start_c(3)));
    % function for acceleration trajectory after interval
    fdda = strcat('0*x+',num2str(end_c(3)));
    
    % function for velocity trajectory before interval
    fdb = strcat( num2str(start_c(3)), '.*x+' , num2str(start_c(2))    );
    % function for velocity trajectory after interval

    fda = strcat( num2str(end_c(3)), '.*x+' , num2str(end_c(2))    );
    % function for position trajectory before interval
    fb = strcat(num2str( (start_c(3)/2) ),'.*x.^2+',num2str(start_c(2)),'.*x+',num2str(start_c(1)));
    % function for position trajectory after interval    
    fa = strcat(num2str(end_c(3)/2),'.*x.^2+',num2str(end_c(2)),'.*x+',num2str(end_c(1)));
    % linspaces array for function (xt), before function (xb), after
    % function (xa)
    xt = linspace(0,time,(time*50)+1);
    xb = linspace(-1,0,11);
    xa = linspace(time, time+1, 11);
    
    close all
    
    % ========
    % POSITION
    % ========
    figure(1);
    % function with . before operators
    % we need xfun to differentiate it with syms, but syms doesn't allow "."
    
    ft = char(xfun);
    
    ft = strrep(ft,'*','.*');
    ft = strrep(ft,'^','.^');
    
    % function calc
    % -------------
    x = xt;
    % y array; eval
    yt = eval(ft);
    
    % out-of-interval lines
    % ---------------------
    % before
    x = linspace(-1,0,11);
    yb = eval(fb);
    % after
    x = linspace(0,1,11);
    ya = eval(fa);
    
    % plot function and hold on
    plot(xb,yb,'r-',xt,yt,'b-',xa,ya,'r-');
    hold on
    
    % window decoration
    axis([-1 time+1 min(yt)-2 max(yt)+2]);
    legend('q(t)');
    ylabel('rad');
    grid on
    
    % ========
    % VELOCITY
    % ========
    figure(2);
    
    % convert sym to string to use strrep
    fdt = char(fd);
   
    % add . operators  
    fdt = strrep(fdt,'*','.*');
    fdt = strrep(fdt,'^','.^');

    % x array
    x = xt;
    
    % y array; eval
    ydt = eval(fdt);
    
    % horizontal lines
    % before
    x = linspace(-1,0,11);
    ydb = eval(fdb);
    
    % after
    x = linspace(0,1,11);
    yda = eval(fda);
    
    % plot
    plot(xb,ydb,'r-', xt,ydt,'b-', xa,yda,'r-');
    hold on
    
    
    % window decoration
    axis([-1 time+1 min(ydt)-2 max(ydt)+2]);
    legend('qd(t)');
    ylabel('rad/s');
    grid on
    
    % ============
    % ACCELERATION
    % ============
    figure(3);
    
    % convert sym to string to use strrep
    fddt = char(fdd);
    
    % add . operators  
    fddt = strrep(fddt,'*','.*');
    fddt = strrep(fddt,'^','.^');

    % y array; eval
    x = xt;
    yddt = eval(fddt);
    
    % horizontal lines
    % before
    x = xb;
    yddb = eval(fddb);
    % after
    x = xa;
    ydda = eval(fdda);
    % plot
    plot(xb,yddb,'r-', xt,yddt,'b-', xa,ydda,'r-');
    hold on
    
    % window decoration
    axis([-1 time+1 min(yddt)-2 max(yddt)+2]);
    legend('qdd(t)');
    ylabel('rad/s^2');
    grid on
    
    hold off   
    
end