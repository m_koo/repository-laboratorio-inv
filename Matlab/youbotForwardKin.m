function [T] = youbotForwardKin(q)

    youbot_DH;
    
    T = eye(4);
    
    for k=1:5
        theta = q(k) + youbot.offset(k);
        d = youbotDH(k,2);
        a = youbotDH(k,3);
        alpha = youbotDH(k,4);
        CTM = [ cos(theta)  -sin(theta)*cos(alpha)  sin(theta)*sin(alpha)   a*cos(theta);
                sin(theta)  cos(theta)*cos(alpha)   -cos(theta)*sin(alpha)  a*sin(theta);
                0           sin(alpha)              cos(alpha)              d;
                0           0                       0                       1];
        T = T * CTM;
    end
   
%     fprintf('Iteration result\n');
%     
%     theta1 = q(1);
%     theta2 = q(2);
%     theta3 = q(3);
%     theta4 = q(4);
%     theta5 = q(5);
%     
%     d1 = youbotDH(1,2);
%     d5 = youbotDH(5,2);
%     
%     a1 = youbotDH(1,3);
%     a2 = youbotDH(2,3);
%     a3 = youbotDH(3,3);
%     
%     alpha1 = youbotDH(1,4);
%     alpha4 = youbotDH(4,4);
    
%     fprintf('Fixed formula result\n');
%     
%     T = [   sin(theta1)*sin(theta5) + cos(theta2 + theta3 + theta4)*cos(theta1)*cos(theta5),     cos(theta5)*sin(theta1) - cos(theta2 + theta3 + theta4)*cos(theta1)*sin(theta5),         sin(theta2 + theta3 + theta4)*cos(theta1),       cos(theta1)*(a1 + a3*cos(theta2 + theta3) + a2*cos(theta2)) + d5*sin(theta2 + theta3 + theta4)*cos(theta1);
%             cos(theta2 + theta3 + theta4)*cos(theta5)*sin(theta1) - cos(theta1)*sin(theta5),     - cos(theta1)*cos(theta5) - cos(theta2 + theta3 + theta4)*sin(theta1)*sin(theta5),       sin(theta2 + theta3 + theta4)*sin(theta1),       sin(theta1)*(a1 + a3*cos(theta2 + theta3) + a2*cos(theta2)) + d5*sin(theta2 + theta3 + theta4)*sin(theta1);
%             sin(theta2 + theta3 + theta4)*cos(theta5),                                           -sin(theta2 + theta3 + theta4)*sin(theta5),                                              -cos(theta2 + theta3 + theta4),                  d1 + a3*sin(theta2 + theta3) + a2*sin(theta2) - d5*cos(theta2 + theta3 + theta4);
%         	0,                                                                                   0,                                                                                       0,                                               1];

    youbot.plot(q);
        
end
