% % Test trajectory A
% % =================

q0 = [pi/3 pi/4 -pi/5 1 2];
q1 = [-pi/6 pi/3 0.5 -pi/2 -1];
T0 = youbot.fkine(q0);
T1 = youbot.fkine(q1);

% % Test trajectory B
% % =================
% 
% q0 = [-pi/4 pi/2 -pi/4 0 1];
% q1 = [-pi/4 pi/4 -pi/3 -0.6 3];
% T0 = youbot.fkine(q0);
% T1 = youbot.fkine(q1);

% % Test trajectory C
% % =================

% T0 = [  0.2588   -0.0000    0.9659    0.4481;
%         0.0000   -1.0000   -0.0000   -0.0000;
%         0.9659    0.0000   -0.2588    0.2958;
%         0         0         0         1.0000];  
% qs = youbotInverseKin(T0);
% q0 = qs(1,:);
% T1 = [  0.2588   -0.0000    0.9659    0.4481;
%         0.0000   -1.0000   -0.0000   -0.0000;
%         0.9659    0.0000   -0.2588    0.0;
%         0         0         0         1.0000];
% qs = youbotInverseKin(T1);
% q1 = qs(1,:);

% TC = ctraj(T0, T1, 100);
% 
% for k=1:100
%     q = youbotInverseKin(TC(:,:,k));
%     Q(k,:) = q(1,:);
% end
% 
% youbot.plot(Q(1,:));
% hold on
% 
% for k=1:100
%     youbot.plot(Q(k,:));
%     plot3(TC(1,4,k), TC(2,4,k), TC(3,4,k), 'r.')
% end

Q = jtraj(q0, q1, 100);
youbot.plot(Q(1,:));
hold on

for k=1:100
    youbot.plot(Q(k,:));
    T = youbot.fkine(Q(k,:));
    plot3(T(1,4), T(2,4), T(3,4), 'r.')
end


hold off